SET datestyle = "ISO, DMY";
CREATE SCHEMA IF NOT EXISTS schema_name;
CREATE TABLE IF NOT EXISTS schema_name.table_name(
  id SERIAL PRIMARY KEY,
  param1 VARCHAR(250) NOT NULL,
  param2 VARCHAR(250) NOT NULL,
  param3 DATE NOT NULL,
  created_at DATE NOT NULL,
  modified_at DATE,
  deleted_at DATE
);
CREATE TABLE IF NOT EXISTS schema_name.another_table_name(
  id SERIAL PRIMARY KEY,
  param1 VARCHAR(250) NOT NULL,
  param2 VARCHAR(250) NOT NULL,
  param3 DATE NOT NULL,
  created_at DATE NOT NULL,
  modified_at DATE,
  deleted_at DATE
);
CREATE TABLE IF NOT EXISTS shoppingcart_schema.break_table(
  table_id INTEGER NOT NULL,
  an_table_id INTEGER NOT NULL,
  quantity INTEGER NOT NULL,
  FOREIGN KEY(table_id) REFERENCES shoppingcart_schema.table_name(id),
  FOREIGN KEY(an_table_id) REFERENCES shoppingcart_schema.another_table_id(id)
);
