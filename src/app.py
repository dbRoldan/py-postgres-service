# -*- coding: utf-8 -*-
import os
from dotenv import load_dotenv
from crud import ObjectDAO
from connection import Connection

# Config
load_dotenv()
db_config = ("dbname="+os.getenv('DB_NAME')
    + " user=" + os.getenv('DB_USER')
    + " host=" + os.getenv('DB_HOST')
    + " port=" + os.getenv('DB_PORT')
    + " password="+ os.getenv('DB_PASSWORD'))
db_tables_path = os.getenv('DB_TABLES_PATH')

#DB Connection
connection = Connection(db_config)
res = connection.createTables(db_tables_path)
print('Tables Creation: {}'.format('Executed Scripts' if res else 'Had not executed'))

#DAO Objects
objectControl = ObjectDAO(connection)

if __name__ == "__main__":
    ### Here you can to use any crud method from ObjectControl
    filter = {
        'and': [
            {
                'condition': '=',
                'param': 'COMPARISON_PARAM_NAME',
                'value': "'DATA'"
            }
            ],
        'or':[
            {
                'condition': '!=',
                'param': 'COMPARISON_PARAM_NAME',
                'value': "'DATA'"
            }
        ]}
    object = objectControl.findByFilter(filter)
    print('Search with filter:  {}'.format(object))

    print('Search without filter:  {}'.format(objectControl.findAll()))
