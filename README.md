# PostgreSQL Project
### Use

This is a basic CRUD service project for PostgreSQL, for use this:
* Change your table name in **ObjectDAO** in *crud.py*
* Enter your data connection to the database to the development environment.
* Call method than you need, with a *db_config*
* The definition of filters is made in a dynamic way in order to facilitate their implementation according to the case, specified as:
```
filter = {
      'and': [
          {
              'condition': '=',
              'param': 'COMPARISON_PARAM_NAME',
              'value': "'DATA'"
          }
          ],
      'or':[
          {
              'condition': '!=',
              'param': 'COMPARISON_PARAM_NAME',
              'value': "'DATA'"
          }
      ]}
```

### Recommendations
For security remember to previously create the database, the user in charge of it, and finally the pricilegios for them:
To edit the project you can use the comment inside the project folder:
~~~~sql
CREATE DATABASE db_name;
CREATE USER user_name with ENCRYPTED PASSWORD 'paswword';
GRANT ALL PRIVILEGES ON database db_name TO user_name;
~~~~

### License
MIT License
